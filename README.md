# Python + C++ SWIG example.

- Install [swig](http://www.swig.org/exec.html) first (like `yum install swig` or `apt-get install swig`)

- Install python-devel package (something like `python2-devel` or `python3-devel`) 

- Run `make` (Check `Makefile` for the details)

- Run `python check_repeat.py`:
```
$ $ python check_repeat.py
hellohellohellohellohello
```

## Python 3 notes

- Replace `-I/usr/include/python2.7/` in Makefile with something like `-I/usr/include/python3.7m/`
(Check where your python include files are located).

## Links

[SWIG and Python](http://www.swig.org/Doc1.3/Python.html)

[Using SWIG to wrap C++ for Python](https://www.cs.ubc.ca/~gberseth/blog/using-swig-to-wrap-c-for-python.html)

[About vectors of doubles](http://www.swig.org/Doc1.3/Library.html#Library_nn15)
