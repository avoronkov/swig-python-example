.PHONY: all clean

all:
	swig -c++ -python repeat.i
	g++ -fpic -c repeat.cpp repeat_wrap.cxx -I/usr/include/python2.7/
	gcc -shared repeat_wrap.o repeat.o -o _repeat.so -lstdc++

clean:
	rm -f repeat.py *.o *.so *_wrap.cxx *.pyc
	rm -rf __pycache__/
