#pragma once

#include <string>
#include <vector>

std::string repeat(const std::string & str, int times);

double sum_doubles(const std::vector<double> &vd);
