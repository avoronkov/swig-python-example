%module repeat

%{
#define SWIG_FILE_WITH_INIT
#include "repeat.h"
%}

%include "std_string.i"
%include "std_vector.i"
namespace std {
   %template(IntVector) vector<int>;
   %template(DoubleVector) vector<double>;
}
%include "repeat.h"
