#!/usr/bin/env python

import repeat

print(repeat.repeat("hello", 5))

d = repeat.DoubleVector([1.1, 0.5, 2.0])
print(repeat.sum_doubles(d))
