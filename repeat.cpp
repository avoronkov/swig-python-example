#include "repeat.h"

#include <sstream>

std::string repeat(const std::string & str, int times) {
	std::ostringstream result;
	for (int i = 0; i < times; i++) {
		result << str;
	}
	return result.str();
}

double sum_doubles(const std::vector<double> &vd) {
    double result = 0.0;
    for(double d : vd) {
        result += d;
    }
    return result;
}
